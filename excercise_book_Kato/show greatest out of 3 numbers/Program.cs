﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace show_greatest_out_of_3_numbers
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Podaj pierwszą liczbę: ");
            int x = int.Parse(Console.ReadLine());
            Console.WriteLine("Podaj drugą liczbę: ");
            int y = int.Parse(Console.ReadLine());
            Console.WriteLine("Podaj trzecią liczbę: ");
            int z = int.Parse(Console.ReadLine());

            if (x > y)
            {
                if (x > z)
                {
                    Console.WriteLine("Największą liczbą jest " + x);
                }
                else
                {
                    Console.WriteLine("Największą liczbą jest " + z);
                }
            }
            else if (y > z)
            {
                Console.WriteLine("Największą liczbą jest " + y);
            }
            else
            {
                Console.WriteLine("Największą liczbą jest " + z);
            }
            Console.ReadKey();
        }
    }
}
