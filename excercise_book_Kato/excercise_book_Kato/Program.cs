﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace excercise_book_Kato
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("podaj długość boku kwadratu: ");
            int numberOfRows = int.Parse(Console.ReadLine());
            int numberOfColumns = numberOfRows;
            do
            {
                for (int i = numberOfColumns; i != 0; i--)
                {
                    Console.Write("* ");
                }
                Console.WriteLine();
                numberOfRows--;
            } while (numberOfRows !=0);
            Console.ReadKey();
        }
    }
}

