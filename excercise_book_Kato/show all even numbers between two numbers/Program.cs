﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace show_all_even_numbers_between_two_numbers
{
    class Program
    {
        static void Main(string[] args)
        {
            int x, y;
            Console.WriteLine("Podaj pierwsza liczbe calkowita");
            x = int.Parse(Console.ReadLine());
            Console.WriteLine("Podaj druga liczbe calkowita");
            y = int.Parse(Console.ReadLine());
            if (x > y)
            {
                do
                {
                    if (x % 2 == 0)
                    {
                        Console.WriteLine(x);
                    }
                    x--;
                } while (x != y);
            }
            else if (y > x)
            {
                do
                {
                    if (y % 2 == 0)
                    {
                        Console.WriteLine(y);
                    }
                    y--;
                } while (y != x);
            }
            else
            {
                Console.WriteLine("Oba numery sa rowne");
            }
            Console.ReadKey();
        }
    }
}
