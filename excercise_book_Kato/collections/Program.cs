﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace collections
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] uczestnicy = new int[] { 19, 34, 23, 54, 31 };
            int[] odwrotnie = new int[uczestnicy.Length];
            int sum = 0;

            for (int i = uczestnicy.Length - 1; i >= 0; i--)
            {
                odwrotnie[uczestnicy.Length - i - 1] = uczestnicy[i]; // - i - 1 --> tu rośnie indeks tablicy odwrotnie
            }
            Console.WriteLine(odwrotnie[0]);
            Console.WriteLine(odwrotnie[1]);
            Console.WriteLine(odwrotnie[2]);
            Console.WriteLine(odwrotnie[3]);
            Console.WriteLine(odwrotnie[4]);
            Console.ReadKey();

            foreach (var i in uczestnicy)
            {
                sum += i;
            }
            double average = (double)sum / uczestnicy.Length;
            Console.WriteLine("Średnia to: {0}", average);
            Console.ReadKey();

        }
    }
}
