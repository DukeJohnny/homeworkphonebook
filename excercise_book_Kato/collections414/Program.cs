﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace collections414
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] identyfikatory = new string[3];
            int year = 0;
            int hyphenIndex = 0;
            int yearDifference = 0;
            for (int i = 0; i < identyfikatory.Length; i++)
            {
                Console.WriteLine("Wpisz identyfikator");
                identyfikatory[i] = Console.ReadLine();
            }
            foreach (string i in identyfikatory)
            {
                hyphenIndex = i.IndexOfAny("-".ToCharArray());
                year = int.Parse(i.Substring(++hyphenIndex));
                yearDifference = 2019 - year;
                Console.WriteLine("identyfikator: {0}, rok: {1}, różnica lat: {2}", i, year, yearDifference);
            }
            Console.ReadKey();
        }
    }
}
