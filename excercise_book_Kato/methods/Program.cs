﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace methods
{
    class Program
    {
        static void NaszaMetoda(int a)
        {
            a++;
            Console.WriteLine("argument z wnetrza metody: " + a);
        }
        static void Main(string[] args)
        {
            int x = 5;
            Console.WriteLine("przed wywolaniem: " + x);
            NaszaMetoda(x);
            Console.WriteLine("po wywolaniu metody: " + x);
            Console.ReadKey();
        }
    }
}
