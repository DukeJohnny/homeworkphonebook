﻿using System;

namespace PhoneBook
{
    public class ReadWriteHelper
    {
        public void WriteToUser (string stringToWrite)
        {
            Console.WriteLine(stringToWrite);
        }

        public string ReadFromUser()
        {
            string rawInput = Console.ReadLine();
            return rawInput;
        }

        public void ClearConsole()
        {
            Console.Clear();
        }

        public void PrintPhoneBookItem(PhoneBookItem item)
        {
            Console.WriteLine(item.Name);
            Console.WriteLine(item.Number.ToString());
            Console.WriteLine(item.CityName);
            Console.WriteLine("");
        }
    }

}
