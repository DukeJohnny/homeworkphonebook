﻿using System;

namespace PhoneBook
{
    public class InputValidation : ReadWriteHelper
    {
        public string Operation { get; set; }
        public string InputName { get; set; }
        public int InputNumber { get; set; }
        public string InputCity { get; set; }


        public bool CheckAll()
        {
            if (CheckName() == true && CheckNumber() == true && CheckCity() == true)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool CheckOperation()
        {
            string input = ReadFromUser();
            if (string.IsNullOrEmpty(input) == true)
            {
                return false;
            }
            else
            {
                Operation = input.ToLower();
                return true;
            }
        }

        public bool CheckName()
        {
            WriteToUser("Please enter Name:");
            string input = Console.ReadLine();
            while (string.IsNullOrEmpty(input) == true)
            {
                WriteToUser("The name is null or empty, try again.");
                input = Console.ReadLine();
                if (input.ToLower() == "exit")
                {
                    break;
                }
            }
            if (input.ToLower() == "exit")
            {
                return false;
            }
            else
            {
                InputName = input;
                return true;
            }
        }

        public bool CheckNumber()
        {
            WriteToUser("Please enter number:");
            string input = Console.ReadLine();
            while (string.IsNullOrEmpty(input) == true || input.Length != 9 || int.TryParse(input, out int outputInt) != true)
            {
                if (input.ToLower() == "exit")
                {
                    break;
                }
                WriteToUser("The input provided is either: null, empty, doesn't have exactly 9 digits or it's not a number, try again.");
                input = Console.ReadLine();
            }
            if (input.ToLower() == "exit")
            {
                return false;
            }
            else
            {
                InputNumber = int.Parse(input);
                return true;
            }
        }

        public bool CheckCity()
        {
            WriteToUser("Please enter city name:");
            string input = Console.ReadLine();
            while (string.IsNullOrEmpty(input))
            {
                WriteToUser("The city name is null or empty, try again.");
                input = Console.ReadLine();
                if (input.ToLower() == "exit")
                {
                    break;
                }
            }
            if (input.ToLower() == "exit")
            {
                return false;
            }
            else
            {
                InputCity = input;
                return true;
            }
        }
    }
}
