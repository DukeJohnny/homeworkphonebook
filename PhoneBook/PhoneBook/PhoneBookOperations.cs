﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace PhoneBook
{
    public class PhoneBookOperations
    {
        private ReadWriteHelper uiInteractions;
        private static List<PhoneBookItem> phoneBookEntries = new List<PhoneBookItem>();

        public PhoneBookOperations()
        {
            uiInteractions = new ReadWriteHelper();
        }
        public void OperationOnBook(InputValidation input)
        {
            uiInteractions.ClearConsole();
            string operation = input.Operation.ToLower();
            switch (operation)
            {
                case "add":
                    Adding(input);
                    return;
                case "edit":
                    Editing(input);
                    return;
                case "delete":
                    Deleting();
                    return;
                case "list":
                    Listing();
                    return;
                case "find":
                    Finding();
                    return;
                case "exit":
                    return;
                default:
                    uiInteractions.WriteToUser("Operation not recognized!");
                    return;
            }
        }
        public void Adding(InputValidation input)
        {
            if (!input.CheckAll()) return;
            var newItem = new PhoneBookItem(input.InputName, input.InputNumber, input.InputCity);
            phoneBookEntries.Add(newItem);
            uiInteractions.ClearConsole();
            uiInteractions.WriteToUser("New contact added.");
        }
        public void Editing(InputValidation input)
        {
            uiInteractions.ClearConsole();
            if (phoneBookEntries.Any())
            {
                uiInteractions.WriteToUser("Please type a name of contact to edit and hit 'ENTER': ");
                string nameToFind = uiInteractions.ReadFromUser();
                var elementToChange = phoneBookEntries.Find(e => string.Equals(e.Name, nameToFind, StringComparison.CurrentCultureIgnoreCase));
                if (elementToChange == null)
                {
                    uiInteractions.ClearConsole();
                    uiInteractions.WriteToUser("No such entry to edit found.");
                }
                else
                {
                    uiInteractions.WriteToUser("Please provide new data for the contact:");
                    if (!input.CheckAll()) return;
                    elementToChange.Name = input.InputName;
                    elementToChange.Number = input.InputNumber;
                    elementToChange.CityName = input.InputCity;
                }
            }
            else
            {
                uiInteractions.WriteToUser("You have no friends. There are no entries to edit.");
            }
        }
        public void Deleting()
        //Unfortunately, for now I was able only to delete ALL entries that match the query.
        {
            uiInteractions.ClearConsole();
            if (!phoneBookEntries.Any())
            {
                uiInteractions.WriteToUser("The phone book is already empty. Try adding some entries maybe?");
            }
            else
            {
                uiInteractions.WriteToUser("Please type a name of contact to delete and hit 'ENTER': ");
                string nameToFind = uiInteractions.ReadFromUser();
                List<PhoneBookItem> matchingNames = phoneBookEntries.FindAll(e => nameToFind != null && e.Name.ToLower() == nameToFind.ToLower());
                if (matchingNames.Count == 0)
                {
                    uiInteractions.ClearConsole();
                    uiInteractions.WriteToUser("No such entry to delete found.");
                }
                else
                {
                    phoneBookEntries.Remove(matchingNames.Find(e => nameToFind != null && e.Name.ToLower() == nameToFind.ToLower()));
                    uiInteractions.WriteToUser("The first found entry matching the given name has been deleted.");
                }
            }
        }
        public void Finding()
        {
            uiInteractions.ClearConsole();
            if (phoneBookEntries.Count == 0)
            {
                uiInteractions.WriteToUser("The phone book is empty, you have no friends, go meet some, lol.");
            }
            else
            {
                uiInteractions.WriteToUser("Please type a name of contact to find and hit 'ENTER': ");
                string nameToFind = uiInteractions.ReadFromUser();
                List<PhoneBookItem> matchingNames = phoneBookEntries.FindAll(e => nameToFind != null && e.Name.ToLower() == nameToFind.ToLower());
                if (nameToFind != null && nameToFind.ToLower() == "exit") return;

                if (matchingNames.Any())
                {
                    uiInteractions.ClearConsole();
                    uiInteractions.WriteToUser("Entries matching the given name:" + Environment.NewLine);
                    foreach (var entry in matchingNames)
                    {
                        uiInteractions.PrintPhoneBookItem(entry);
                    }
                }
                else
                {
                    uiInteractions.ClearConsole();
                    uiInteractions.WriteToUser("No entries found.");
                }
            }
        }
        public void Listing()
        {
            uiInteractions.ClearConsole();
            if (phoneBookEntries.Any())
            {
                uiInteractions.WriteToUser("All entries of the phone book:");
                List<PhoneBookItem> sortedEntriesList = phoneBookEntries.OrderBy(o => o.Name).ToList();
                foreach (var entry in sortedEntriesList)
                {
                    uiInteractions.PrintPhoneBookItem(entry);
                }
            }
            else
            {
                uiInteractions.WriteToUser("The phone book is empty, please add new entries. (Seriously, go get some friends.)");
            }
        }
    }
}