﻿using System;

namespace PhoneBook
{

    class Program
    {
        static void Main(string[] args)
        {
            var readWriteHelper = new ReadWriteHelper();
            string quittingCommand;
            do
            {
                readWriteHelper.WriteToUser("Available operations: Find, Add, Delete, Edit, List (lists all entries of the book), Exit");
                readWriteHelper.WriteToUser("Please enter operation:");

                var userInput = new InputValidation();
                if (userInput.CheckOperation() && userInput.Operation != "exit")
                {
                    var operation = new PhoneBookOperations();
                    operation.OperationOnBook(userInput); 
                }
                else
                {
                    readWriteHelper.WriteToUser("The operator is null or empty!" + Environment.NewLine);
                }
                readWriteHelper.WriteToUser("Hit 'ENTER' to return to main menu or type 'exit' to quit.");
                quittingCommand = readWriteHelper.ReadFromUser()?.ToLower();
                readWriteHelper.ClearConsole();

            } while (quittingCommand != "exit");
        }
    }
}