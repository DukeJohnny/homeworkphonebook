﻿namespace PhoneBook
{
    public class PhoneBookItem
    {
        public string Name { get; set; }
        public int Number { get; set; }
        public string CityName { get; set; }

        public PhoneBookItem(string nameParam, int numberParam, string cityNameParam)
        {
            Name = nameParam;
            Number = numberParam;
            CityName = cityNameParam;
        }
    }
    
}
